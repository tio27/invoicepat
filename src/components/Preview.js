import React, { Component } from 'react'

class Preview extends Component {
    

    currency = (v=0,type="text") => {
        var b = v.toString().split('.'),
            val = b[0],
            c1 = b.length <= 1 ? ",00" : b[1]=="00"? ",00":","+b[1],
            min = v < 0 ? '-':'',
            n = parseInt(val.replace(/\D/g,''),0).toLocaleString(),
            c = n.replace(/,/g ,'.') == "NaN" ? "" : n.replace(/,/g ,'.'),
            cur = type === "text" ? this.props.data.currency: "";
        c1 = type ==="text" ? c1 : "";
        return min+cur+c+c1;
      }
    dateFormat = (date) =>{
        const dateTimeFormat = new Intl.DateTimeFormat('en', { year: 'numeric', month: '2-digit', day: '2-digit' });
        const [{ value: month },,{ value: day },,{ value: year }] = dateTimeFormat .formatToParts(date );
        return `${month}/${day}/${year}`
    }

    textFormat = (txt) => {
        var t = txt.replace(/\n/g, '<br />\n');
        return (
            <p dangerouslySetInnerHTML={{__html: t}}></p>
        )
    }
    render(){
        const data = this.props.data,item = this.props.item;
        const items = [];
        Object.keys(item).map(function(i){
            let {price,name,cost,qty,desc,withDesc,tax,disc,discType,discPercent} = item[i];
            let txs = Object.keys(tax);
            items.push(
                <div className="ib_item" key={"key-"+i}>
                    <div className="row" >
                        
                        <div className="col-md-5 col-6">
                           {name}
                           <div className="hidden-xs">
                            {withDesc && (
                                    <div className="txt-grey">{this.textFormat(desc)}</div>
                                )}
                           </div>
                             
                        </div>
                        <div className="col-md-3 col-4">
                            {this.currency(cost)}
                        </div>
                        <div className="col-md-1 col-2">
                            {qty}
                        </div>
                        <div className="view-xs col-12 order-md-last pt-0">
                            {withDesc && (
                                <div className="txt-grey">{this.textFormat(desc)}</div>
                            )}
                        </div>
                        <div className="col-md-3">
                            <span className="view-xs">Price: </span>  {this.currency(price)}
                            {txs.map(function(t){
                                let {name,percent,ammount} = tax[t];
                                return (
                                <div className="txt-grey" key={t}>{name} {percent}% ({this.currency(ammount)})</div>
                                )
                            }.bind(this))}
                            
                            {disc && (
                                <div className="txt-grey">Discount 
                                    {discType === "percent" ? " "+discPercent+"%: "+this.currency(disc) : ": "+this.currency(disc)}
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            )
        }.bind(this))
        return(
            <div className="ib_box preview">
                <div className="ib_top">
                    <div className="row">
                        <div className="col-md-3 col-5">
                            <span>Invoice #{data.invoiceID}</span>
                        </div>
                        <div className="col-md-9 text-right col-7">
                            <span>Date issued : {data.dateIssued}</span>
                            <span>Date due: {data.dateDue}</span>
                           
                        </div>
                    </div>
                </div>
                
                <div className="ib_head ib_section">
                    <div className="row">
                        <div className="col-6 order-2">
                            <h2>Invoice</h2>
                        </div>
                        <div className="col-6">
                            {data.files!=="" && (
                                <div className="uploader">
                                    <div className="uploader_img" style={{ backgroundImage:`url(${data.files})` }}></div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                <hr />
                <div className="ib_addr ib_section">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="form-group group">
                                <label>Bill from:</label>
                                <p>
                                    {data.billFrom !=="" && (<>{data.billFrom}<br /></>)}
                                    {data.companyFrom !=="" && (<>{data.companyFrom}<br /></>)}
                                    {data.countryFrom !=="" && (<>{data.countryFrom}<br /></>)}
                                    {data.emailFrom !=="" && (<>{data.emailFrom}<br /></>)}
                                    {data.phoneFrom !=="" && (<>{data.phoneFrom}<br /></>)}
                                </p>
                                
                                
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form-group group">
                                <label>Bill to:</label>
                                <p>
                                    {data.billTo !=="" && (<>{data.billTo}<br /></>)}
                                    {data.companyTo !=="" && (<>{data.companyTo}<br /></>)}
                                    {data.countryTo !=="" && (<>{data.countryTo}<br /></>)}
                                    {data.emailTo !=="" && (<>{data.emailTo}<br /></>)}
                                    {data.phoneTo !=="" && (<>{data.phoneTo}<br /></>)}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="ib_items ib_section">
                    
                    <div className="row ib_items_head">
                        <div className="col-md-5 col-6">Item</div>
                        <div className="col-md-3 col-4">Cost</div>
                        <div className="col-md-1 col-2">Qty</div>
                        <div className="col-3 hidden-xs">Price</div>
                    </div>
                    <div className="ib_items_wrap">
                        {items}

                    </div>
                    
                </div>
                <div className="ib_foot ib_section">
                    <div className="row">
                        <div className="col-md-6">
                            
                                {this.textFormat(data.clientNote)}
                            
                        </div>
                        <div className="col-md-6 ib_foot_detail">
                            <div className="row justify-content-between">
                                <div className="col-auto">Subtotal</div>
                                <div className="col-auto">{this.currency(data.subtotal)}</div>
                            </div>
                            {data.withDiscWhole && (
                                <div className="row justify-content-between">
                                    <div className="col-auto">Discount</div>
                                    <div className="col-auto">{this.currency(data.discWhole)}</div>
                                </div>
                            )}
                            
                            {Object.keys(item).map(function(i){
                                
                            
                                return Object.keys(item[i].tax).map(function(t){
                                    let {name,ammount} = item[i].tax[t];
                                    
                                    return <div className="row justify-content-between" key={t}>
                                        <div className="col-auto">{name}</div>
                                        <div className="col-auto">{this.currency(ammount)}</div>
                                    </div>
                                }.bind(this))
                            }.bind(this))}
                            <hr />
                            <div className="row justify-content-between ib_foot_total">
                                <div className="col-auto">Invoice total ({data.currencyCode})</div>
                                <div className="col-auto">{this.currency(data.total)}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Preview;