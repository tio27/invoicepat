import React, { Component } from 'react'
import { Page, Text, View, Document, StyleSheet, Image, Font } from '@react-pdf/renderer';

import Graphik from '../fonts/Graphik-Regular.ttf';
import GraphikBold from '../fonts/Graphik-Bold.ttf';

// Create styles
const purple = '#5E5083',grey = '#999';

const registerFont = () =>{
    Font.register({
        family: 'Graphik',
        fonts: [
          {
            src: Graphik
          },
          {
            src: GraphikBold,
            fontWeight: 'bold'
          }
        ]
      })
}

const styles = StyleSheet.create({
  page: {
    padding: 40,
    backgroundColor: '#FFF',
    fontSize: 9,
    lineHeight: 1.5,
    //fontFamily: 'Graphik'
  },
  section: {
  	width: '50%'
  },
  txtRight: {
    textAlign: 'right'
  },
  sectionRight: {
    width: '50%',
    textAlign: 'right'
  },
  full: {
      width: '100%',
      backgroundColor: '#F00',
      color: '#FFF',
      padding: 10
  },
  head:{
    flexDirection: 'row',
    //display: 'flex',
    //flexWrap: 'wrap',
    height: 100,
  },
  headImg:{
      maxWidth: 100,
      maxHeight: 100,
      objectFit: 'contain',
      objectPosition: 'center',
      
  },    
  h1:{
      fontSize: 36,
      fontWeight: 'bold',
      margin: '30 0 0 0',
      color: '#5E5083',
      textAlign: 'right'
  },
  detail:{
    flexDirection: 'row',
    //display: 'flex',
    //flexWrap: 'wrap',
    padding: '30 0',
    borderBottom: '1pt solid #ccc'
    
  },
  txtBold:{
    fontWeight: '800'
  },
  info: {
    flexDirection: 'row',
    padding: '30 0'
  },
  infoh3:{
    fontWeight: 'bold'
  },
  infoh2:{
    fontSize: 18,
    fontWeight: 'bold',
    color: purple
  },
  row:{
    flexDirection: 'row',
    borderBottom: '1 solid #ccc',
    padding: '5',
  },
  rowTitle: {
  	backgroundColor: '#ddd',
    fontWeight: 'bold',
    border: 'none'
  },
  col1:{
    width: '41.667%'
  },
  col2:{
    width: '25%'
  },
  col3:{
    width: '8.33%'
  },
  col4:{
    width: '25%'
  },
  txtSmall:{
    fontSize: 9
  },
  m10:{
    marginTop: 5
  },
  txtGrey:{
    color: grey
  },
  foot:{
    flexDirection: 'row'
  },
  footCol1:{
    width: '64.667%',
    padding: '10 5 5'
  },
  footCol2:{
    width: '35.33%',
    padding: 5
  },
  footRow:{
    flexDirection: 'row',
    padding: '5 0',
    //borderBottom: '1 solid #ccc'
  },
  footTotal:{
    padding: '10 0',
    border: 'none',
    borderTop: "1 solid #ccc"
  },
  txtTotal:{
    fontSize: 12
  }


});

class MyDocument extends Component {
      
      currency = (v=0,type="text") => {
        var b = v.toString().split('.'),
            val = b[0],
            c1 = b.length <= 1 ? ",00" : b[1]=="00"? ",00":","+b[1],
            min = v < 0 ? '-':'',
            n = parseInt(val.replace(/\D/g,''),0).toLocaleString(),
            c = n.replace(/,/g ,'.') == "NaN" ? "" : n.replace(/,/g ,'.'),
            cur = type === "text" ? this.props.data.currency: "";
        c1 = type ==="text" ? c1 : "";
        return min+cur+c+c1;
      }
    render(){
        const data = this.props.data,item = this.props.item;
        return(
            <Document>
                <Page size="A4" style={styles.page}>
                    <View style={styles.head}>
                        <View style={styles.section}>
                            {data.files!=="" &&(
                                <Image src={data.files} style={styles.headImg} />
                            )}
                        </View>
                        <View style={styles.section}>
                            <Text style={styles.h1}>Invoice</Text>
                        </View>
                    </View>
                    <View style={styles.detail}>
                        <View style={styles.section}>
                            <Text style={styles.txtBold}>Bill From:</Text>
                            {data.billFrom!=="" && (
                                <Text>{data.billFrom}</Text>
                            )}
                            {data.companyFrom!=="" && (
                                <Text >{data.companyFrom}</Text>
                            )}
                            {data.countryFrom!=="" && (
                                <Text>{data.countryFrom}</Text>
                            )}
                            {data.emailFrom!=="" && (
                                <Text >{data.emailFrom}</Text>
                            )}
                            {data.phoneFrom!=="" && (
                                <Text >{data.phoneFrom}</Text>
                            )}
                        </View>
                        <View style={styles.sectionRight}>
                        <Text style={styles.txtBold}>Bill to:</Text>
                            {data.billTo!=="" && (
                                <Text>{data.billTo}</Text>
                            )}
                            {data.companyTo!=="" && (
                                <Text >{data.companyTo}</Text>
                            )}
                            {data.countryTo!=="" && (
                                <Text>{data.countryTo}</Text>
                            )}
                            {data.emailTo!=="" && (
                                <Text >{data.emailTo}</Text>
                            )}
                            {data.phoneTo!=="" && (
                                <Text >{data.phoneTo}</Text>
                            )}
                        </View>
                    </View>
                    <View style={styles.info}>
                        <View style={styles.section}>
                            <Text >Invoice #{data.invoiceID}</Text>
                            <Text >Issued: {data.dateIssued}</Text>
                            <Text >Due: {data.dateDue}</Text>
                        </View>
                        <View style={styles.sectionRight}>
                            <Text style={styles.infoh3}>INVOICE TOTAL</Text>
                            <Text style={styles.infoh2}>{this.currency(data.total)}</Text>
                        </View>
                    </View>
                    <View style={[styles.row,styles.rowTitle]}>
                        <View style={styles.col1}>
                            <Text>ITEM</Text>
                        </View>
                        <View style={styles.col2}>
                            <Text>COST</Text>
                        </View>
                        <View style={styles.col3}>
                            <Text>QTY</Text>
                        </View>
                        <View style={[styles.col4,styles.txtRight]}>
                            <Text>PRICE</Text>
                        </View>
                    </View>
                    {Object.keys(item).map(function(i){
                        let {price,name,cost,qty,desc,withDesc,tax,disc,discType,discPercent} = item[i];
                        let txs = Object.keys(tax);
                        return(
                            <View style={styles.row} key={i}>
                                <View style={styles.col1}>
                                    <Text>{name}</Text>
                                    <View style={styles.m10}>
                                        <Text style={[styles.txtSmall,styles.txtGrey]}>{desc}</Text>
                                    </View>
                                </View>
                                <View style={styles.col2}>
                                    <Text>{this.currency(cost)}</Text>
                                </View>
                                <View style={styles.col3}>
                                    <Text>{qty}</Text>
                                </View>
                                <View style={[styles.col4,styles.txtRight]}>
                                    <Text>{this.currency(price)}</Text>
                                    <View style={styles.m10}>
                                        {txs.map(function(t){
                                            let {name,percent,ammount} = tax[t];
                                            return (
                                                <Text style={[styles.txtSmall,styles.txtGrey]} key={t}>{name} {percent}% ({this.currency(ammount)})</Text>
                                            )
                                        }.bind(this))}
                                        {disc && (
                                            <Text style={[styles.txtSmall,styles.txtGrey]}>Discount 
                                            {discType === "percent" ? " of "+discPercent+"%: "+this.currency(disc) : ": "+this.currency(disc)}</Text>
                                        )} 
                                        
                                    </View>
                                </View>
                            </View>
                        )
                    }.bind(this))}
                    
                    <View style={styles.foot}>
                        <View style={styles.footCol1}>
                            {data.clientNote !=="" &&(
                                <>
                                    <Text style={styles.txtBold}>CLIENT NOTE:</Text>
                                    <View style={styles.m10}>
                                        <Text style={[styles.txtSmall,styles.txtGrey]}>{data.clientNote}</Text>
                                    </View>
                                </>
                            )}
                        </View>
                        <View style={styles.footCol2}>
                            <View style={styles.footRow}>
                                <View style={styles.section}>
                                    <Text>Subtotal</Text>
                                </View>
                                <View style={[styles.section,styles.txtRight,styles.txtBold]}>
                                    <Text>{this.currency(data.subtotal)}</Text>
                                </View>
                            </View>
                            {Object.keys(item).map(function(i){
                                
                            
                                return Object.keys(item[i].tax).map(function(t){
                                    let {name,ammount} = item[i].tax[t];
                                    console.log(name,ammount);
                                    return (
                                        <View style={styles.footRow} key={t}>
                                            <View style={styles.section}>
                                                <Text>{name}</Text>
                                            </View>
                                            <View style={[styles.section,styles.txtRight,styles.txtBold]}>
                                                <Text>{this.currency(ammount)}</Text>
                                            </View>
                                        </View>
                                    )
                                }.bind(this))
                            }.bind(this))}
                            {data.withDiscWhole && (
                                <View style={styles.footRow}>
                                    <View style={styles.section}>
                                        <Text>Discount</Text>
                                    </View>
                                    <View style={[styles.section,styles.txtRight,styles.txtBold]}>
                                        <Text>{this.currency(data.discWhole)}</Text>
                                    </View>
                                </View>
                            )}
                            
                            <View style={[styles.footRow,styles.footTotal]}>
                                <View style={styles.section}>
                                    <Text>Invoice Total ({data.currencyCode})</Text>
                                </View>
                                <View style={[styles.section,styles.txtRight,styles.txtBold,styles.txtTotal]}>
                                    <Text>{this.currency(data.total)}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </Page>
            </Document>
        )
    }
}

export default MyDocument;