
import React, { Component, lazy, Suspense } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { Loadings, BarLoadings } from "../components/Loading";
import { country, dataCurrency, masterDataCountry, masterDataCurrency } from "../utils/Country";
//import 'rsuite/dist/styles/rsuite-default.css';

import $ from "jquery";
import DragAndDrop from "../components/DragAndDrop";


import MyDocument from "../components/Document";
import { PDFDownloadLink } from "@react-pdf/renderer";
import { myFirebase } from "../firebase/firebase";
import * as firebase from 'firebase/app'
import { DatePicker, Select, Spin } from "antd";
import moment from "moment";
const Preview = lazy(() => import('../components/Preview'));
require('bootstrap');

const { Option } = Select;
const dpFormat = "MM/DD/YYYY";

class Home extends Component {
  state = { 
      invoice:{
        invoiceID:"1",dateIssued:"",dateDue:"",billFrom:"",companyFrom: "",countryFrom: "Indonesia",emailFrom:"",phoneFrom:"",billTo:"",companyTo:"",countryTo:"Indonesia",emailTo:"",phoneTo:"",subject:"",message:"",
        currency: "Rp",
        currencyCode: "IDR",
        files:"",
        discWhole: 0,
        discWholeAmmount: 0,
        discWholeType: 'percent',
        withDiscWhole: false,
        clientNote: "",
        subtotal: 0,
        total: 0,
      },
      id: "",
      errorFileMsg: "",
      discWholeMsg: "",
      isPreviewed: false,
      emailFromMsg: false,
      emailToMsg: false,
      isEditing: true,
      isLoading: false,
      isSaving: false,
      isGenerated: false,
      item:[
        {name: "",
        cost: 0,
        qty: 1,
        price: 0,
        desc: "",
        withDesc: false,
        tax:{},
        taxAmmount:"",
        discAmmount:"",
        discMsg:"",
        discType:'percent', 
        disc:false,
        discPercent:0,
        }
      ],
  };
  temp = {
        listCurrency: [],
      listCountry: [],
  }
  
  constructor(props){
    super(props)
    this.baseState = this.state;
    
    
  }

  componentWillMount(){
        var di = moment(),
            dd = moment().add('15','days'),
            lt = this.state,
            tt = this.temp;
        tt.di = di;
        tt.dd = dd;
        this.setState({
            dateDue: dd,
            dateIssued: di,
            invoice:{
                ...this.state.invoice,
                dateIssued:di.format(dpFormat),
                dateDue:dd.format(dpFormat)
            }
        });

        Object.keys(masterDataCurrency).map(function(i){
            const data = masterDataCurrency[i];
            tt.listCurrency.push({value:i,label:data.name})
        })
        Object.keys(masterDataCountry).map(function(i){
            const data = masterDataCountry[i];
            tt.listCountry.push({value:data.name,label:data.name})
        })

        tt.listCountry2 = tt.listCountry;

        //this.setState({listCurrency:lt.listCurrency,listCountry: lt.listCountry})


  }

  createNew = (e) =>{
      e.preventDefault();
      var di = moment(),
          dd = moment().add('15','days');
      this.baseState.dateIssued= di;
      this.baseState.dateDue = dd;
      this.baseState.invoice.dateIssued = di.format(dpFormat);
      this.baseState.invoice.dateDue = dd.format(dpFormat);
      this.baseState.item = [
        {name: "",
        cost: 0,
        qty: 1,
        price: 0,
        desc: "",
        withDesc: false,
        tax:{},
        taxAmmount:"",
        discAmmount:"",
        discMsg:"",
        discType:'percent', 
        disc:false,
        discPercent:0,
        }
      ];
      this.setState(this.baseState);
      
  }

  sendSlack = () =>{
    var datas = new FormData(),
        lt = this.state,lti = lt.invoice;
    datas.append('to',lti.emailTo);
    datas.append('id',lt.id);
    datas.append('from',lti.emailFrom);
    datas.append('fromName',lti.billFrom);
    
    
    //console.log(blob);
    fetch('https://invoicepat.com/backend/api/sendSlack',{
      method: "POST",
      //mode: "no-cors",
      headers: {
          //"Content-Type": "application/json",
          "cache-control": "no-cache",
        },
      //body: JSON.stringify(data)
      body:datas
    }).then(response => response.json())
    .then(parsedData=>{
        console.log('sukses');
        
    })
    .catch(error => {
      console.log(error);
      
    });
  }
  saveToDB = (type="email") =>{         
      var db = myFirebase.firestore(),
          //batch = db.batch(),
          th = this,
          ts = th.state,
          ref = ts.id===""? db.collection('invoice').doc(): db.collection('invoice').doc(ts.id),
          tts = ts,
          data = {};
    
      data = this.state.invoice;
      /*batch.set(ref,data);
      Object.keys(ts.item).map(function(i){
          var itemRef = db.collection('invoice').doc(id).collection('item').doc();
          
          batch.set(itemRef,ts.item[i])
      })
      batch.commit();*/
      this.setState({isSaving: true,isGenerated: true})
      //console.log(ts.id);
      //console.log(type);
      if(ts.id===""){
        
        ref.set(data).then(function(){
            ref.update({
                created: firebase.firestore.Timestamp.now(),
                dateIssued: firebase.firestore.Timestamp.fromDate(ts.dateIssued.toDate()),
                dateDue: firebase.firestore.Timestamp.fromDate(ts.dateDue.toDate()),
                item: ts.item
            })
            th.setState({isSaving: false,id:ref.id})
            th.sendSlack()
            
            
        }).catch(function(err){
            console.log(err);
        })
      }else{
        //console.log('update');
        ref.update(data).then(function(){
            ref.update({
                dateIssued: firebase.firestore.Timestamp.fromDate(ts.dateIssued.toDate()),
                dateDue: firebase.firestore.Timestamp.fromDate(ts.dateDue.toDate()),
                item: ts.item
            })
            th.setState({isSaving: false})
            
        }).catch(function(err){
            console.log(err);
        });
      }
      
  }
  
  Preview = () =>{
      var err = 0;
    $('.ib_box').find('input.required').each(function(){
        if($(this).val()===""){
            $(this).addClass('error');
            if(err ===0){
                $(this).focus();
            }
            err++;
        }else{
            $(this).removeClass('error');
        }
    })

    this.setState({isPreviewed:true})
    

   var lt = this.state;
   if(lt.emailFromMsg){
       $('input[name="emailFrom"]').focus()
   }else if(lt.emailToMsg){
        $('input[name="emailTo"]').focus()
    }
   
    if(err === 0 && !lt.emailFromMsg && !lt.emailToMsg && lt.discWholeMsg===""){
        this.setState({isEditing: false})
    }

    var //nl = "\r\n",
        lti = this.state.invoice,
        //txt = "Dear "+lt.billTo+","+nl+"Thank you for your business, always a pleasure to work with you!"+nl+"We have generated a new invoice in the amount of "+this.currency(lt.total)+nl+"We would appreciate payment of this invoice by "+lt.dateDue,
        txt = "A beautiful invoice has been created for you. You may find a PDF of your invoice attached.",
        subject = lti.billTo+" Invoice #"+lti.invoiceID+" from "+lti.billFrom;

    //txt = lt.message === "" ? txt : lt.message;
    //subject = lt.subject ===""?subject: lt.subject;
    //console.log(this.textFormat(txt));
    this.setState({
        invoice:{
            ...this.state.invoice,
            message:txt,subject: subject
        }
    })
    
  }

  addItem = () =>{
    var st = this.state.item,
        key = Object.keys(st),
        n = key.length,
        err=0,
        data = {
            name: "",
            cost: 0,
            qty: 1,
            price: 0,
            desc: "",
            withDesc: false,
            tax:{},
            taxAmmount:"",
            discAmmount:"",
            discMsg:"",
            discType:'percent', 
            disc:false,
            discPercent:0,
        };
    

      
        $('.ib_items_wrap').find('input.required').each(function(){
            if($(this).val()===""){
                $(this).addClass('error');
                if(err ===0){
                    $(this).focus();
                }
                err++;
            }
        })
      if(err === 0){
        st[this.getTimeStamp()] = data;
        this.setState({item: st});
      }
      
  }

  deleteItem = (i,e) =>{
    e.preventDefault();
    let n = Object.keys(this.state.item).length;
    if(n > 1){
        delete this.state.item[i];
        this.setState({item: this.state.item})
    }
  }
  
  hasClass = (obj,c) => {
    var className = " " + c + " ";
    
    if ((" " + obj.className + " ").replace(/[\n\t]/g, " ").indexOf(className) >= 0) {
            return true;
    }
    return false;
  }

  

  handleChange = e => {
      let obj = e.target,
            v = obj.value,
            el = obj.name,
            valid = true,
            isNum = this.hasClass(obj,'number');
    
        if(isNum){
            if(v!==""){
                v = parseInt(v.replace(/\./g ,''));
                v = isNaN(v) ? this.state[el] : v ;
            }
        }
      if(obj.type === "email"){
        var emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        var isValid = emailRegex.test(v),
            msg = isValid || v==="" ? false : "Invalid email address";
        

        this.setState({
            [el+"Msg"]: msg
        })

      }
      if(true){
        this.setState({
            //invoice[el] : v
            invoice:{
                ...this.state.invoice,
                [el]:v
            }
        })

        let th = this;
        if(isNum){
            setTimeout(function(){
                th.countTotal();
            },100)
        }
      }
  }

  discType = (type) =>{
    this.setState({
        invoice:{
            ...this.state.invoice,
            discWholeType:type
        }
    });

    let th = this;

    setTimeout(function(){
        th.countTotal()
    },200)
    
  }
  
  handleDateIssued = (date,v) =>{
      this.setState({
          dateIssued:date,
          invoice: {
              ...this.state.invoice,
              dateIssued: v
          }
      })
  }
  handleDateDue = (date,v) =>{
    this.setState({
        dateDue:date,
        invoice: {
            ...this.state.invoice,
            dateDue: v
        }
    })
  }
  currency = (v=0,type="text") => {
    var b = v.toString().split('.'),
        val = b[0],
        c1 = b.length <= 1 ? ",00" : b[1]=="00"? ",00":","+b[1],
        min = v < 0 ? '-':'',
        n = parseInt(val.replace(/\D/g,''),0).toLocaleString(),
        c = n.replace(/,/g ,'.') == "NaN" ? "" : n.replace(/,/g ,'.'),
        cur = type === "text" ? this.state.invoice.currency: "";
    c1 = type ==="text" ? c1 : "";
    return min+cur+c+c1;
  }

  

  countTotal = () =>{
    let {item} = this.state,
        {discWhole,discWholeType,discWholeAmmount,withDiscWhole} = this.state.invoice ;
    var taxs = 0,sub=0,total=0;
    //console.log('acount total');
    //count subtotal
    Object.keys(item).map(function(i){
        sub+=parseFloat(item[i]['price'])
        Object.keys(item[i].tax).map(function(t){
            taxs+=parseFloat(item[i].tax[t]['ammount'])
        }.bind(this))
    }.bind(this))
    
    var dw = 0;
    //console.log('type ='+discWholeType)
    
    //setTimeout(function(){
        if(withDiscWhole){

            var th = this,
                dt = discWholeType,
                valid = true,
                v = discWholeAmmount,
                max = dt === 'percent' ? 100 : sub,
                maxLabel = dt === 'percent'? '100%' : th.currency(sub)+" (subtotal)";
            valid = v<=max;
            if(!valid){
                this.setState({discWholeMsg:"The discount value exceeds the maximum amount, which is "+maxLabel});
                //return;
            }else{
                this.setState({discWholeMsg:""})
                if(discWholeType==="percent"){
                    dw = parseFloat(discWholeAmmount/100*sub).toFixed(2)
                }else{
                    dw = discWholeAmmount;
                }
            }

        }else{
            dw = 0;
            discWholeAmmount = 0;
        }
    //},100)

    

    total = sub+taxs-dw;
    

    this.setState({
        invoice:{
            ...this.state.invoice,
            subtotal:sub.toFixed(2),total:total.toFixed(2),discWhole:dw,discWholeAmmount:discWholeAmmount
        }
    })
  }

  count = (i) =>{
      var row = this.state.item[i],
          qty = row['qty'],
          cost = row['cost'],
          price = parseInt(qty*cost),
          dm = "",
          valid = true,
          tax = row.tax,
          discType = row['discType'],
          disc = !row['disc']?false: discType == "percent" ? parseFloat(row['discPercent']/100*price).toFixed(2): row['disc'],priceFinal = 0;
    
    
    
    valid = price>=disc;
    if(!valid){
        disc = false
    }


    
    priceFinal = valid ? parseFloat(price-disc).toFixed(2) : price;
    //console.log(priceFinal,disc)
    Object.keys(tax).map(function(i) {
        tax[i]['ammount'] = parseFloat(tax[i]['percent']/100*priceFinal).toFixed(2);
    }.bind(this));
    this.setState({
        item:{
            ...this.state.item,
            [i]:{
                ...this.state.item[i],
                disc: disc,
                price: priceFinal,
                tax : tax,
                //discMsg: dm
            }
        }
    })

    this.countTotal();
  }

  toggleDiscWhole = (e) =>{
    e.preventDefault();
    this.setState({
        invoice:{
            ...this.state.invoice,
                withDiscWhole: !this.state.invoice.withDiscWhole
        }
    })

  }

  

  
  
  handleItemChange = (i,e) => {
    let obj = e.target,
        v = obj.value,
        el = obj.name,
        valid = true,
        isNum = this.hasClass(obj,'number');
    
    if(isNum){
        if(v!==""){
            v = parseInt(v.replace(/\./g ,''));
            v = isNaN(v) ? this.state.item[i][el] : v ;
        }
    }

    
    

    if(el==="discAmmount" || el === "type"){
        var th = this;
        var  form = $(obj).closest('.ib_item').find('form.formDisc'),
            v2 = form.find('input[name="discAmmount"]').val(),
            row = this.state.item[i],
            discType = form.find('input[name="type"]:checked').val(),
            max = discType === 'percent' ? 100 : row.price,
            dm = "",
            maxLabel = discType === 'percent'? '100%' : th.currency(row.price)+" (price)";
        v2 = parseInt(v2.replace(/\./g ,''));
        v2 = isNaN(v2) ? 0 : v2;
        //console.log(v2,max);
        valid = v2<=max;
        if(!valid){
            dm = "The discount value exceeds the maximum amount, which is "+maxLabel;
            //return;
        }else{
            dm = "";
        }

        this.setState({
            item:{
                ...this.state.item,
                [i]:{
                    ...this.state.item[i],
                    [el]: v,
                    discMsg: dm
                }
            }
        })
        
    }else{
        if(v!==""){
            $(obj).removeClass('error');
        }
        this.setState({
            item:{
                ...this.state.item,
                [i]:{
                    ...this.state.item[i],
                    [el]: v
                }
            }
        })
    }

    
    
    let t = this;
    setTimeout(function(){
        if(isNum){
            t.count(i);
        }
    },50)  
  }

  addDesc = (i,e) =>{
    this.setState({
        item:{
            ...this.state.item,
            [i]:{
                ...this.state.item[i],
                withDesc: true
            }
        }
    })
  }

  remoceDesc = (i,e) =>{
    this.setState({
        item:{
            ...this.state.item,
            [i]:{
                ...this.state.item[i],
                withDesc: false,
                desc: ""
            }
        }
    })
  }

  getTimeStamp = () =>{
      var t = new Date().getTime();
      return t;
    
  }

  addTax = e => {
      e.preventDefault();
      var name = e.target.taxName.value,
          ammount = e.target.taxAmmount.value,
          index = e.target.index.value,
          st = this.state.item[index]['tax'],
          key = Object.keys(st),
          n = key.length,
          data = {
              name: name,
              percent: ammount,
              ammount: parseFloat(ammount/100*this.state.item[index]['price']).toFixed(2)
          };
        
        if(name===""){
            $(e.target.taxName).addClass('error')
        }else if(ammount===""){
            $(e.target.taxAmmount).addClass('error')
        }else{
            $(e.target).find('input.error').removeClass('error');
            st[this.getTimeStamp()] = data;
            this.setState({
                item:{
                    ...this.state.item,
                    [index]:{
                        ...this.state.item[index],
                        taxAmmount: "",
                        tax: st
                    }
                }
            })
            this.countTotal();
            e.target.taxName.value = "";
            e.target.taxAmmount.value="";
            //console.log('adad');
            $('.dropdown-toggle').dropdown('hide');
            e.stopPropagation()
        }
        
        //console.log(this.state.item);

      
  }

  deleteTax = (i,t,e) =>{
    e.preventDefault();
    let txs = this.state.item[i].tax;
    delete txs[t];
    var c = 0;
    this.setState({item: this.state.item});
    this.countTotal();
  }

  addDisc = e =>{
    e.preventDefault();
    var type = e.target.type.value,
        
        index = e.target.index.value,
        row = this.state.item[index],
        ammount = row.discAmmount,
        disc = type == "percent" ? parseFloat(ammount/100*row.price).toFixed(2):ammount,
        discAmmount = type == "percent" ? ammount : 0,
        price = row.price - disc;

    
        if(row.discMsg!==""){
            return
        }
        
        if(ammount === ""){
            $(e.target.discAmmount).addClass('error').focus();
        }else{
            $(e.target.discAmmount).removeClass('error');
            //console.log(index,price,disc,type);
            this.setState({
                item:{
                    ...this.state.item,
                    [index]:{
                        ...this.state.item[index],
                        price: price,
                        disc: disc,
                        discType: type,
                        discPercent: discAmmount,
                        discAmmount: "",
                        discMsg: ""
                    }
                }
            })
            let th = this;
            setTimeout(function(){th.count(index);},100)
            setTimeout(function(){th.countTotal();},200)
      
            e.target.discAmmount.value = "";
            setTimeout(function(){$('.dropdown-toggle').dropdown('hide');},200)
            //
            //console.log(this.state);
        }
      
      
  }

  deleteDisc = (i,e) =>{
      e.preventDefault();
      this.setState({
        item:{
            ...this.state.item,
            [i]:{
                ...this.state.item[i],
                disc: 0,
                price: this.state.item[i].cost*this.state.item[i].qty
            }
        }
    })
    var t = this;
    setTimeout(function(){
        t.count(i);
    },100)
    setTimeout(function(){
        t.countTotal();
    },200)
    
  } 

  isValidSize = (files) =>{
      return files.size <= 2000000;
  }

  handleDrop = (Files,v) => {
      console.log(v);
    let fileList = this.state.files,th = this;
    var reader = new FileReader();
    if(this.isValidSize(Files[0])){
        this.setState({errorFileMsg:""})
        reader.onloadstart = function(data) {
            console.log('start loading');
        }
    
        reader.onload = function(e) {
            th.setState({
                invoice:{
                    ...th.state.invoice,
                    files: e.target.result
                }
            })
        }
    
        reader.readAsDataURL(Files[0]);
    }else{
        this.setState({errorFileMsg:"Image size exceeds the maximum limit of 2MB"})
    }
    
  }

  deleteImg = e=>{
      e.preventDefault();
      this.setState({
        invoice:{
            ...this.state.invoice,
            files: ""
        }
      })
}

  setBlob = (blob) =>{
        var reader = new FileReader();
        var lt = this;
        var contentType = "application/pdf";
        reader.readAsDataURL(blob); 
        reader.onloadend = function() {
            var base64data = reader.result;                
            //console.log(base64data);
            lt.temp.pdfBlob = blob;
        }
      
  }

  textFormat = (txt) => {
    var t = txt.replace(/\n/g, '<br />\n');
    return t;
}

  sendEmail = (e) =>{
      
      e.preventDefault();
      //const blob = pdf(<MyDocument data={this.state}/>).toBlob();
      this.saveToDB();
      const blob =  this.temp.pdfBlob;
      const lt = this.state,lti = lt.invoice;
      var datas = new FormData(),err = 0;
      
      $(e.target).find('.form-control').each(function(i){
        if($(this).val()===""){
            $(this).addClass('error');
            if(err ===0){
                $(this).focus();
            }
            err++;
        }else{
            $(this).removeClass('error')
        }
      })
    
      if(err === 0){
        this.setState({isLoading:true})
        datas.append('to',lti.emailTo);
        datas.append('subject', lti.subject);
        datas.append('blob',blob);
        datas.append('id',lti.invoiceID);
        datas.append('from',lti.emailFrom);
        datas.append('fromName',lti.billFrom);
        datas.append('due',lti.dateDue);
        datas.append('total',this.currency(lti.total));
        datas.append('txt', this.textFormat(lti.message));
  
        //console.log(blob);
        fetch('https://invoicepat.com/backend/api/mail',{
          method: "POST",
          //mode: "no-cors",
          headers: {
              //"Content-Type": "application/json",
              "cache-control": "no-cache",
            },
          //body: JSON.stringify(data)
          body:datas
        }).then(response => response.json())
        .then(parsedData=>{
            console.log('sukses');
            this.setState({isLoading:false})
            $('#modal').modal('hide');
        })
        .catch(error => {
          console.log(error);
          this.setState({isLoading:false})
        });
      }
      
  }   

  setCurrency = (v) =>{
    var cr = masterDataCurrency[v].symbol_native,
        crC = v;
    
    this.setState({
        invoice:{
            ...this.state.invoice,
            currency: cr,currencyCode:crC
        }
    });
    
  }

  setCountry = (n,v) =>{
      this.setState({
          invoice:{
              ...this.state.invoice,
              [n]:v
          }
      })
  }

  
  componentDidUpdate(){
      let th = this;
      $('.btn-download').click(function(){
        th.saveToDB('download')
    
      })
  }
  componentDidMount(){
    let th = this;  
   
    $('.ib_disc_dropdown').on('hidden.bs.dropdown',function(){

        var i = $(this).find('input[name="index"]').val();

        th.setState({
            item:{
                ...th.state.item,
                [i]:{
                    ...th.state.item[i],
                    discAmmount: 0,
                    discMsg: ""
                }
            }
        })
      })
  }
  
  

  render() {
    const {billFrom,item,currency} = this.state;
    const lt = this.state.invoice, ts = this.state;
    const items = [];
    
    //console.log(this.state);
    Object.keys(item).map(function(i){
        let {price,name,cost,qty,desc,withDesc,tax,disc,discType,discPercent,taxAmmount,discAmmount,discMsg} = item[i];
        let txs = Object.keys(tax);
        items.push(
            <div className="ib_item" key={"key-"+i}>
                <div className="row" >
                    <a className="ib_delete" href="#" onClick={e=>this.deleteItem(i,e)}><img src="/asset/images/Delete.svg" /></a>
                    <div className="col-md-5 col-6">
                        <input type="text" autoComplete="off" className="form-control required" placeholder="Item name" name="name" value={name} onChange={e=> this.handleItemChange(i,e)} />
                        
                    </div>
                    <div className="col-md-3 col-4">
                        <input type="text" autoComplete="off" className="form-control number currency" placeholder="0" value={this.currency(cost,'input')} name="cost" onChange={e=> this.handleItemChange(i,e)} />
                    </div>
                    <div className="col-md-1 col-2">
                        <input type="text" autoComplete="off" className="form-control number" placeholder="0" value={qty} name="qty" onChange={e=> this.handleItemChange(i,e)} />
                    </div>
                    <div className='col-12 col-md-5 order-md-last pt-0'>
                    {!withDesc && (<a className="ib_add add_link" onClick={e=>this.addDesc(i,e)}>Add line description</a>)}
                        {withDesc && (
                            <div className="ib_desc">
                                <textarea rows="3" autoComplete="off" className="form-control" placeholder="Item description" value={desc} name="desc" onChange={e=> this.handleItemChange(i,e)} ></textarea>
                                <a className="ib_desc_delete" onClick={e=>this.remoceDesc(i,e)}><img src="/asset/images/Delete.svg" /></a>
                            </div>
                        )}
                    </div>
                    <div className="col-12 order-md-last">
                    <div className="ib_td">
                            <div className="ib_tax">
                                <span>Tax:</span>
                                <div className="ib-col">
                                    {txs.length > 0 && (
                                        <div className='ib_taxs'>
                                            {txs.map(function(t){
                                                let {name,percent,ammount} = tax[t];
                                            return <span className="ib_taxs_item" key={t}><img className="ib_tax_delete" src="/asset/images/cross.svg" onClick={e=>this.deleteTax(i,t,e)} />{name} {percent}% ({this.currency(ammount)})</span>
                                            }.bind(this))}
                                        </div>
                                    )}
                                    
                                    <div className="dropdown ib_dropdown">
                                        <a className="dropdown-toggle" data-toggle="dropdown">Add tax</a>
                                        <div className="dropdown-menu">
                                            <form onSubmit={this.addTax}>
                                            
                                                <input type="hidden" ref="index" name="index" value={i} />
                                                <div className="form-group ns mb-16">
                                                    <input type="text" autoComplete="off" className="form-control" placeholder="Tax name" ref="taxName" name="taxName" />
                                                    
                                                    <div className="input-group">
                                                        <input type="text" autoComplete="off" className="form-control number" placeholder="Tax ammount" ref="taxAmmount" name="taxAmmount" autoComplete="off" value={taxAmmount} onChange={e=> this.handleItemChange(i,e)}/>
                                                        <div className="input-group-append">
                                                            <span className="input-group-text" id="basic-addon2">%</span>
                                                        </div>
                                                        </div>
                                                </div>
                                                <div className="form-action">
                                                    <button type="submit" className="btn">Add tax</button>
                                                    
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ib_disc">
                                <span>Discount:</span>
                                <div className="ib-col">
                                    {!disc ? (
                                        <div className="dropdown ib_dropdown ib_disc_dropdown">
                                            <a className="dropdown-toggle"  data-toggle="dropdown">Add discount</a>
                                            <div className="dropdown-menu">
                                                <form className="formDisc" onSubmit={this.addDisc}>
                                                    <input type="hidden" ref="index" name="index" value={i} />
                                                    <div className="form-group mb-8">
                                                        <label className="radio">
                                                            <input name="type" value="percent" onChange={e=> this.handleItemChange(i,e)} type="radio" defaultChecked />
                                                            <div>By percentage (%)</div>
                                                        </label>
                                                        <label className="radio">
                                                            <input name="type" value="flat" onChange={e=> this.handleItemChange(i,e)} type="radio" />
                                                            <div>By flat ({lt.currency})</div>
                                                        </label>
                                                    </div>
                                                    <div className="form-group mb-16">
                                                        <input type="text" autoComplete="off" className={discMsg===""?"form-control number":"form-control number error"} placeholder="Discount ammount" ref="discAmmount" name="discAmmount" onChange={e=> this.handleItemChange(i,e)}
                                                        value={this.currency(discAmmount,'input')} />
                                                        {discMsg!=="" && (<div className="errorMsg">{discMsg}</div>)}
                                                    </div>
                                                    <div className="form-action">
                                                    
                                                        <button type="submit" className="btn">Apply discount</button>
                                                        
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    ):(
                                        <div className='ib_taxs'>
                                            <span className="ib_taxs_item white"><img className="ib_tax_delete" src="/asset/images/cross.svg" onClick={e=>this.deleteDisc(i,e)} />
                                            {discType === "percent" ? discPercent+"% ("+this.currency(disc)+")" : this.currency(disc)}</span>
                                        
                                        </div>
                                    )}
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3"><span className="view-xs txt-semibold">Price : </span>{this.currency(price)}</div>
                </div>
            </div>
        )
    }.bind(this))
    //console.log(item);
    return (
    <>
        <div className="sc1">
            <div className="container">
                
                <h1>Beautiful invoice. <br />Every time.</h1>
                <p>Send beautiful invoice to your client without effort.<br className="hidden-xs" /> Invoice that look professional, invoice on-the-go & get paid faster. Invoices made easy!</p>
                
            </div>
        </div>
        <div className="ib">
            <div className="container">
                {ts.isEditing ? (
                    <>
                        <div className="ib_box">
                            <div className="ib_top">
                                <div className="row">
                                    <div className="col-lg-3 col-md-4 col-5">
                                        <span>Invoice #</span>
                                        <span><input autoComplete="off" className="form-control required w72" type="text" name="invoiceID" value={lt.invoiceID} onChange={this.handleChange} /></span>
                                    </div>
                                    <div className="col-lg-9 col-md-8 text-right col-7">
                                        <div className="ib_topc">
                                            <span>Date issued</span>
                                            <div className="input-calendar">
                                                <DatePicker  value={ts.dateIssued} size='large' format={dpFormat} onChange={(date,v) =>{this.handleDateIssued(date,v)}} />
                                            </div>
                                        </div>
                                            
                                        
                                        <div className="ib_topc">
                                            <span>Date due</span>
                                            <div className="input-calendar">
                                                <DatePicker value={ts.dateDue} format={dpFormat} size='large' onChange={(date,v) =>{this.handleDateDue(date,v)}} />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div className="ib_head ib_section">
                                <div className="row">
                                    <div className="col-6 order-2">
                                        <h2>Invoice</h2>
                                    </div>
                                    <div className="col-6">
                                        <DragAndDrop handleDrop={this.handleDrop} errorMsg ={ts.errorFileMsg}>
                                            {lt.files !=="" && (
                                                <>
                                                    <div className="uploader-clear" onClick={this.deleteImg}>
                                                        <img src="/asset/images/Delete.svg" />
                                                    </div>
                                                    <div className="uploader_img" style={{ backgroundImage:`url(${lt.files})` }}></div>
                                                </>
                                            )}
                                        </DragAndDrop>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div className="ib_addr ib_section">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="form-group group">
                                            <label>Bill from:</label>
                                            <input className="form-control required" type="text" placeholder="Your company name" name="billFrom" value={lt.billFrom} onChange={this.handleChange} />
                                            <textarea  className="form-control" placeholder="Company address (optional)" value={lt.companyFrom} name="companyFrom" onChange={this.handleChange} ></textarea>
                                            <Select defaultValue={lt.countryFrom} style={{width: '100%',textAlign:"left"}} size="large" showSearch placeholder="Search..." filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} onChange={v=>{this.setCountry('countryFrom',v)}}>
                                                {
                                                    Object.keys(this.temp.listCountry).map(function(i){
                                                        const {value,label} = this.temp.listCountry[i];
                                                        return (
                                                            <Option value={value} key={i}>{label}</Option>
                                                        )
                                                    }.bind(this))
                                                }
                                            </Select>
                                            <input className={ts.emailFromMsg?"form-control error format-error required":"form-control required"} value={lt.emailFrom} type="email" placeholder="Email address" name="emailFrom" onChange={this.handleChange} />
                                            {ts.emailFromMsg && (
                                                <div className="errorMsg">{ts.emailFromMsg}</div>
                                            )}
                                            <input autoComplete="off" className="form-control" type="text" placeholder="Phone number (optional)" name="phoneFrom" value={lt.phoneFrom} onChange={this.handleChange}  />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group group">
                                            <label>Bill to:</label>
                                            <input className="form-control required" type="text" placeholder="Customer name" name="billTo" value={lt.billTo} onChange={this.handleChange}  />
                                            <textarea  className="form-control" placeholder="Customer Company address (optional)" name="companyTo" value={lt.companyTo} onChange={this.handleChange}></textarea>
                                            <Select defaultValue={lt.countryTo} style={{width: '100%',textAlign:"left"}} size="large" showSearch placeholder="Search..." filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} onChange={v=>{this.setCountry('countryTo',v)}}>
                                            {
                                                    Object.keys(this.temp.listCountry2).map(function(i){
                                                        const {value,label} = this.temp.listCountry2[i];
                                                        return (
                                                            <Option value={value} key={'country'+i}>{label}</Option>
                                                        )
                                                    }.bind(this))
                                                }
                                            </Select>
                                            <input className={ts.emailToMsg?"form-control error format-error required":"form-control required"} type="email" placeholder="Email address" name="emailTo" value={lt.emailTo} onChange={this.handleChange} />
                                            {ts.emailToMsg && (
                                                <div className="errorMsg">{ts.emailToMsg}</div>
                                            )}
                                            <input autoComplete="off" className="form-control" type="text" placeholder="Phone number (optional)" name="phoneTo" value={lt.phoneTo}  onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="text-right ib_section ib_currency">
                                <Select defaultValue="IDR" style={{width: 220,textAlign:"left"}} size="large" showSearch placeholder="Search..." filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} onChange={v=>{this.setCurrency(v)}}>
                                    {
                                        Object.keys(this.temp.listCurrency).map(function(i){
                                            const {value,label} = this.temp.listCurrency[i];
                                            return (
                                                <Option value={value} key={i}>{label}</Option>
                                            )
                                        }.bind(this))
                                    }
                                </Select>
                                
                            </div>
                            <div className="ib_items ib_section">
                                <div className="row ib_items_head">
                                    <div className="col-md-5 col-6">Item</div>
                                    <div className="col-md-3 col-4">Cost</div>
                                    <div className="col-md-1 col-2">Qty</div>
                                    <div className="col-3 hidden-xs">Price</div>
                                </div>
                                <div className="ib_items_wrap">
                                    {items}
                                    <a className="btn btn-add" onClick={this.addItem}>Add row</a>
                                </div>
                            
                            </div>
                            <hr />
                            <div className="ib_foot ib_section">
                                <div className="row">
                                    <div className="col-md-6">
                                        <textarea name="clientNote" placeholder="Client notes (optional)" autoComplete="off" className="form-control" value={lt.clientNote} onChange={this.handleChange} rows="6"></textarea>
                                    </div>
                                    <div className="col-md-6 ib_foot_detail">
                                        <div className="row justify-content-between">
                                            <div className="col-auto">Subtotal</div>
                                            <div className="col-auto">{this.currency(lt.subtotal)}</div>
                                        </div>
                                        <div className="row justify-content-between">
                                            <div className="col-auto">Discount</div>
                                            <div className="col-auto">{this.currency(lt.discWhole)}</div>
                                        </div>
                                        <div className="ib_foot_disc row justify-content-between">
                                            <div className="col-auto">
                                                <a className={lt.withDiscWhole?"add_link hidden":"add_link"} onClick={this.toggleDiscWhole}>Discount entire invoice</a>
                                            </div>
                                            {lt.withDiscWhole && (
                                                <div className="col-auto">
                                                    <div className="ib_foot_discbox">
                                                        <img src="/asset/images/Delete.svg" className="ib_foot_discdel" onClick={this.toggleDiscWhole} />
                                                        <input type="text" name="discWholeAmmount" className={ts.discWholeMsg!==""?"form-control number error":"form-control number"} onChange={this.handleChange} value={this.currency(lt.discWholeAmmount,'input')}  />
                                                        <div className="ib_foot_disctgl">
                                                            <span className={lt.discWholeType =="percent"? "active":""} onClick={e=>{this.discType('percent')}} data-toggle="tooltip" title="By percentage" >%</span>
                                                            <span className={lt.discWholeType =="percent"? "":"active"} onClick={e=>{this.discType('flat')}}data-toggle="tooltip" title="By flat">{lt.currency}</span>
                                                        </div>
                                                        {ts.discWholeMsg!=="" && (
                                                            <div className="errorMsg">{ts.discWholeMsg}</div>
                                                        )}
                                                    </div>
                                                    
                                                </div>
                                            )}
                                        </div>
                                        {Object.keys(item).map(function(i){
                                            
                                        
                                            return Object.keys(item[i].tax).map(function(t){
                                                let {name,ammount} = item[i].tax[t];
                                                
                                                return <div className="row justify-content-between" key={t}>
                                                    <div className="col-auto">{name}</div>
                                                    <div className="col-auto">{this.currency(ammount)}</div>
                                                </div>
                                            }.bind(this))
                                        }.bind(this))}
                                        <hr />
                                        <div className="row justify-content-between ib_foot_total">
                                            <div className="col-auto">Invoice total ({lt.currencyCode})</div>
                                            <div className="col-auto">{this.currency(lt.total)}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ib_action">
                            <div></div>
                            <a className="btn" onClick={this.Preview}>Preview Invoice</a>
                        </div>
                    </>
                ):
                    <>
                        <Suspense fallback={(<div className="page-loading"><Spin size="large" /></div>)}>
                        <Preview data={lt} item={ts.item}></Preview>
                        <div className="ib_action">
                            <div>
                                {ts.isGenerated && (
                                    <a className="btn btn-white" onClick={this.createNew}>Create new invoice</a>
                                )}
                            </div>
                            <div>
                                <a className="btn btn-white" onClick={e=>{this.setState({isEditing: true})}}>Back to edit</a>
                                
                                <PDFDownloadLink document={<MyDocument data={lt} item={ts.item}  />} fileName={"invoice #"+lt.invoiceID+".pdf"} className="btn btn-white btn-download" >
                                {({ blob, url, loading, error }) => {
                                        // Do whatever you need with blob here
                                        
                                        if(!loading){
                                            this.setBlob(blob);
                                            //console.log(blob);
                                            return "Download now";
                                            
                                        }else{
                                            return "Please wait..."
                                        }
                                    }}
                                </PDFDownloadLink>
                                <a className="btn btn" data-toggle="modal" href="#modal">Email Invoice</a>
                            </div>
                            
                            
                            
                            
                        </div>
                        </Suspense>
                        
                        
                    </>
                }
                
                
                
            </div>
        </div>
        <div className="modal modal-rate modal-toast fade" id="modal" tabIndex="-1" role="dialog">
              <div className="modal-dialog">
                  <div className="modal-success modal-content">
                      <a className="modal-close" data-dismiss="modal">
                          <img src="/asset/images/times-gray.svg" />
                      </a>
                      <div className="modal-heading">Email Invoice</div>
                      <form onSubmit={this.sendEmail}>
                        <div className="form-group">
                            <label>From</label>
                            <input autoComplete="off" className="form-control required" type="text" placeholder="Your company name" name="emailFrom" value={lt.emailFrom} onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label>To</label>
                            <input autoComplete="off" className="form-control required" type="text" placeholder="Your company name" name="emailTo" value={lt.emailTo} onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label>Subject</label>
                            <input autoComplete="off" className="form-control required" value={lt.subject} type="text" placeholder="Invoice..." name="subject" onChange={this.handleChange} />
                        </div>
                        <div className="form-group">
                            <label>Message</label>
                            <textarea rows='10' autoComplete="off" className="form-control required" type="text" placeholder="We have generated invoice..." name="message" value={lt.message} onChange={this.handleChange}  ></textarea>
                        </div>
                        <div className="text-center mt-32">
            <button type="submit" className={ts.isLoading? "btn loading":"btn"}>{ts.isLoading ? "Please wait...":"Send Invoice"}</button>
                        </div>
                      </form>
                      
                  </div>
              </div>
          </div>
    </>        
    );
    
  }
}

/*function mapStateToProps(state) {
  return {
    
  };
}

export default (connect(mapStateToProps)(Home));*/
export default Home;