
import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { loginUser, loginReset } from "../actions";

import $ from "jquery";
import Title from "antd/lib/typography/Title";
import { Input } from "antd";

require('bootstrap')


class Login extends Component {
  state = { email: "", password: "" };

  handleEmailChange = ({ target }) => {
    this.setState({ email: target.value });
  };

  handlePasswordChange = ({ target }) => {
    this.setState({ password: target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { loginError, dispatch } = this.props;
    const { email, password } = this.state;
    dispatch(loginUser(email, password, e.target));
  };

  closeAlert = (e) =>{
    e.preventDefault();
    this.props.dispatch(loginReset());
  }

  render() {
    const { classes, loginError, isAuthenticated } = this.props;
    console.log(this.state);
    if (isAuthenticated) {
      return <Redirect to="/admin/" />;

    } else {
      return (
        <main className="login-page">
          <div className="account-bg">
            <div className="account-wrap">
              <div className="logo"><img src="asset/images/logo.png" width="100" /></div>
              <div className="box">
                
                <Title level={3}>Welcome Back,</Title>
                <p>Sign in to continue</p>
                <form className="login" onSubmit={this.handleSubmit}>
                  {loginError && (
                    <div className="alert alert-warning"><a className="close" onClick={this.closeAlert} >×</a><strong>Oops! </strong>Email and password combination is not valid!</div>
                  )}
                  <div className="form-group">
                    <Input className="form-control" placeholder="Email Address" onChange={this.handleEmailChange} />
                    
                  </div>
                  <div className="form-group">
                    <Input.Password placeholder="Password" className="form-control" size='large' onChange={this.handlePasswordChange} />
                  
                  </div>
                  <div className="form-action">
                    <button className="btn" onClick={this.handleSubmit}>Sign in</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </main>

        
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    isLoggingIn: state.auth.isLoggingIn,
    loginError: state.auth.loginError,
    isAuthenticated: state.auth.isAuthenticated
  };
}

export default (connect(mapStateToProps)(Login));