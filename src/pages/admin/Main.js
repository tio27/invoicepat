import { Layout, Menu, Spin, Dropdown } from 'antd';
import {
  DashboardOutlined,
  DatabaseOutlined,
  DownOutlined
} from '@ant-design/icons';
import React, { Component, lazy, Suspense } from "react";
import { BrowserRouter as Router, Route, Switch, Link, Redirect } from 'react-router-dom';
import Avatar from 'antd/lib/avatar/avatar';
import { logoutUser } from '../../actions';
import { connect } from 'react-redux';
const Invoice = lazy(()=> import('./Invoice/'))
const Dashboard = lazy(()=> import('./Dashboard/'))



const { Header, Content, Footer, Sider } = Layout;
const {SubMenu,Item} = Menu;



class Main extends Component{
    
    handleLogout = (e) => {
        e.preventDefault();
        console.log('logout');
	    const { dispatch } = this.props;
        dispatch(logoutUser());
  	};
    render(){
        const {mainPage, subPage } = this.props.match.params;
        const selectedKey = subPage ? subPage : mainPage;
        return(
            <Layout className="admin-page">
                <Sider theme="light"
                style={{
                    overflow: 'auto',
                    height: '100vh',
                    position: 'fixed',
                    left: 0,
                }}
                >
                    <div className="ant-layout-logo">
                        <img src="/asset/images/logo-big.svg" />

                    </div>
                    <Menu theme="light" mode="inline" defaultOpenKeys={[mainPage]} selectedKeys={[selectedKey]}>
                        
                        <Item key="dashboard" icon={<DashboardOutlined />}>
                            
                            <Link to='/admin'> Dashboard </Link>
                        </Item>
                        <SubMenu key="invoice" icon={<DatabaseOutlined />} title="Invoice" >
                            <Item key="all">
                                <Link to='/admin/invoice'>Invoice</Link>
                            </Item>
                            <Item key="sender">
                                <Link to="/admin/invoice/sender">Sender</Link>
                            </Item>
                        </SubMenu>
                    </Menu>
                </Sider>
                <Layout className="site-layout" style={{ marginLeft: 200 }}>
                    <Header theme="light" className="site-layout-background" style={{ padding: 0 }}>
                        <div className="header-right">
                            <Dropdown overlay={(
                                <Menu>
                                    <Item>
                                        <a href="#" onClick={this.handleLogout}>Logout</a>
                                    </Item>
                                </Menu>
                            )} className="user-dropdown">
                                <a href="#" onClick={e => e.preventDefault()}>
                                    <Avatar src="/asset/images/user.png" />
                                    <span>Administrator</span> <DownOutlined />
                                </a>
                            </Dropdown>
                            
                        </div>

                    </Header>
                    <Content>
                        
                        <Suspense fallback={<div className="page-loading" ><Spin size="large" /></div>}>
                            <Switch>
                                <Route exact path='/admin/invoice/:subPage?/:sub2Page?' component={Invoice} />
                                <Route path="/admin/dashboard" component={Dashboard} />
                                <Redirect to="/admin/dashboard" />
                            </Switch>
                        </Suspense>
                            
                        
                    </Content>
                    
                </Layout>
            </Layout>
        )
    }
}

function mapStateToProps(state) {
    return {
      isLoggingOut: state.auth.isLoggingOut,
      logoutError: state.auth.logoutError
    };
  }

  
  export default connect(mapStateToProps)(Main);