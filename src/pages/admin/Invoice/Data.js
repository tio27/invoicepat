import React, { Component, lazy, Suspense } from "react";
import Title from "antd/lib/typography/Title";
import { Switch, Route, Redirect, Link } from "react-router-dom";
import { myFirebase } from "../../../firebase/firebase";
import { Table, Menu, Breadcrumb } from "antd";
import moment from "moment";
    
class DataInvoice extends Component{
    state = {data:[]};
    column = [
        {
            title: 'Creataed',
            dataIndex: 'created',
            key: 'created',
            defaultSortOrder: 'descend',
            sorter: (a, b) => moment(a.created,'MM/DD/YYYY hh:mm:ss').unix() - moment(b.created,'MM/DD/YYYY hh:mm:ss').unix(),
        },
        {
            title: 'From',
            dataIndex: 'from',
            key: 'from',
            sorter: (a, b) => a.from.localeCompare(b.from)
        },
        {
            title: 'To',
            dataIndex: 'to',
            key: 'to',
            sorter: (a, b) => a.to.localeCompare(b.to)
        },
        {
            title: 'Date Issued',
            dataIndex: 'dateIssued',
            key: 'dateIssued',
            sorter: (a, b) => moment(a.dateIssued,'MM/DD/YYYY').unix() - moment(b.dateIssued,'MM/DD/YYYY').unix(),
        },
        {
            title: 'Date Due',
            dataIndex: 'dateDue',
            key: 'dateDue',
            sorter: (a, b) => moment(a.dateDue,'MM/DD/YYYY').unix() - moment(b.dateDue,'MM/DD/YYYY').unix(),
        },
        {
            title: 'Total',
            dataIndex: 'total',
            key: 'total',
            align: 'right',
            render: (text,r,i) =>{
                return r.currencyCode+" "+this.currency(text)
            }
        },
        {
            title: "Action",
            key: 'action',
            width: 120,
            render: text =>{
                return(
                    <a className="btn btn-white btn-small">Show</a>
                )
            }
        }
        
    ]


    constructor(props){
        super(props);

        this.ref = myFirebase.firestore().collection('invoice').orderBy('created','desc');
        this.unsub = null;
    }

    dateFormat = (date, format = "MM/DD/YYYY") =>{
        return moment(date.seconds*1000).format(format)
    }

    docUpdate = (querySnapshot) =>{
        console.log('docUpdate');
        const data = [];
        querySnapshot.forEach(doc => {
            const {created,emailTo,billTo,emailFrom,billFrom,currencyCode,dateIssued,dateDue,total} = doc.data();

            data.push({
                key : doc.id,
                created: this.dateFormat(created,"MM/DD/YYYY hh:mm:ss"),
                to: billTo+" ("+emailTo+")",
                from: billFrom+" ("+emailFrom+")",
                total: total,
                currencyCode,
                dateIssued: this.dateFormat(dateIssued),
                dateDue : this.dateFormat(dateDue)
            })

            
        });

        this.setState({data:data})
    }

    currency = (v=0,type="input") => {
        var b = v.toString().split('.'),
            val = b[0],
            c1 = b.length <= 1 ? ",00" : b[1]=="00"? ",00":","+b[1],
            min = v < 0 ? '-':'',
            n = parseInt(val.replace(/\D/g,''),0).toLocaleString(),
            c = n.replace(/,/g ,'.') == "NaN" ? "" : n.replace(/,/g ,'.'),
            cur = type === "text" ? this.state.invoice.currency: "";
        //c1 = type ==="text" ? c1 : "";
        return min+cur+c+c1;
      }
    componentDidMount(){
        this.unsub  = this.ref.onSnapshot(this.docUpdate);
        
    }

    render(){
        const { subPage } = this.props.match.params;
        console.log(this.state.data);
        return(
            <>
                <div className="section">
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            Invoice
                            
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                            All Invoice
                        </Breadcrumb.Item>
                    </Breadcrumb>
                    <Title level={2}>All Invoice</Title>
                </div>
                <div className="section">
                    <Table dataSource={this.state.data} columns={this.column} 
                        onRow={(record,rowIndex) =>{
                            return {
                                onClick : e => {
                                    this.props.history.push('/admin/invoice/show/'+record.key)
                                }
                            }
                        }}
                    />
                </div>
            </>
        )
    }
}

export default DataInvoice;