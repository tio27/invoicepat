import React, { Component, lazy, Suspense } from "react";
import Title from "antd/lib/typography/Title";
import { Switch, Route, Redirect, Link } from "react-router-dom";
import { myFirebase } from "../../../firebase/firebase";
import { Table, Spin, Breadcrumb } from "antd";
import moment from "moment";
import { PDFDownloadLink } from "@react-pdf/renderer";
import MyDocument from "../../../components/Document";
import {
    LeftOutlined
  } from '@ant-design/icons';

const Preview = lazy(()=> import('../../../components/Preview'));

class Show extends Component{
    state = {data:{},isLoading: true};
    isId = false;


    constructor(props){
        super(props);
        const { invoiceId } = this.props.match.params;
        if(!invoiceId){
           return this.props.history.push('/admin/invoice');
        }
        this.isId = true;
        this.ref = myFirebase.firestore().collection('invoice').doc(invoiceId)
        this.unsub = null;
        
    }

    dateFormat = (date, format = "MM/DD/YYYY") =>{
        return moment(date.seconds*1000).format(format)
    }

    docUpdate = (querySnapshot) =>{
        var dd = querySnapshot.data();
        
        dd.dateIssued = this.dateFormat(dd.dateIssued);
        dd.dateDue = this.dateFormat(dd.dateDue)
        this.setState({data:dd,isLoading:false})
       
    }
    componentDidMount(){
        if(this.isId){
            this.unsub = this.ref.onSnapshot(this.docUpdate)
        }
    }

    render(){
        const { invoiceId } = this.props.match.params;
        const  lt = this.state.data;
        console.log(lt);
        
        return (
            <>
                {this.state.isLoading ? (
                    <div className="page-loading"><Spin size="large" /></div>
                ) : (
                    <Suspense fallback={(<div className="page-loading"><Spin size="large" /></div>)}>
                        <div className="section">
                            <Breadcrumb>
                                <Breadcrumb.Item>
                                    Invoice
                                    
                                </Breadcrumb.Item>
                                <Breadcrumb.Item>
                                    <Link to="/admin/invoice/all">All Invoice</Link>
                                </Breadcrumb.Item>
                                <Breadcrumb.Item>
                                    Invoice Detail
                                </Breadcrumb.Item>
                            </Breadcrumb>
                            <Title level={2}>Invoice Detail</Title>
                        </div>
                        <Preview data={this.state.data} item={this.state.data.item} />
                        <div className="ib_action">
                            <Link to="/admin/invoice" className='btn btn-white'>Back</Link>
                            
                            <PDFDownloadLink document={<MyDocument data={lt} item={lt.item}  />} fileName={"invoice #"+lt.invoiceID+".pdf"} className="btn btn-download" >
                            {({ blob, url, loading, error }) => {
                                    // Do whatever you need with blob here
                                    
                                    if(!loading){
                                        //this.setBlob(blob);
                                        //console.log(blob);
                                        return "Download now";
                                        
                                    }else{
                                        return "Please wait..."
                                    }
                                }}
                            </PDFDownloadLink>
                            
                        </div>
                    </Suspense>
                )}
            </>
        )

        
    }
}

export default Show;