import React, { Component, lazy, Suspense } from "react";
import Title from "antd/lib/typography/Title";
import { Row, Col, Card, Skeleton, Tabs, Spin } from "antd";
import { myFirebase } from "../../../firebase/firebase";
import $ from "jquery";
import { Link } from "react-router-dom";

const Chart1 = lazy(()=>import('./Chart1'))
const Chart2 = lazy(()=>import('./Chart2'))
const Chart3 = lazy(()=>import('./Chart3'))
const { TabPane } = Tabs;
class Dashboard extends Component{
    state={data:[],invoice:0,invoiceValue:0,sender:0,isLoading: true}

    constructor(props){
        super(props)
        this.ref = myFirebase.firestore().collection('invoice');
        this.unsub = null;
    }

    hasValue = (array,val) => {
        if($.inArray(val,array) !== -1 ){
          return true;
        }else{
          return false;
        }
    }

    currency = (v=0,type="input") => {
        var b = v.toString().split('.'),
            val = b[0],
            c1 = b.length <= 1 ? ",00" : b[1]=="00"? ",00":","+b[1],
            min = v < 0 ? '-':'',
            n = parseInt(val.replace(/\D/g,''),0).toLocaleString(),
            c = n.replace(/,/g ,'.') == "NaN" ? "" : n.replace(/,/g ,'.'),
            cur = type === "text" ? this.state.invoice.currency: "";
        //c1 = type ==="text" ? c1 : "";
        return min+cur+c+c1;
    }

    docUpdate = (querySnapshot) =>{
        const data = [],email=[];
        let i=querySnapshot.size,iV = 0,s=0;
        querySnapshot.forEach(doc => {
            const {created,emailFrom,total} = doc.data();
            var dd = doc.data();
            dd.id = doc.id;
            iV = parseFloat(iV)+parseFloat(total);
            data.push(dd);
            if(!this.hasValue(email,emailFrom)){
                email.push(emailFrom);
                
            }
            
        });
        this.setState({data:data,invoice:i,invoiceValue:iV,sender:email.length,isLoading:false})
    }

    componentDidMount(){
        this.unsub  = this.ref.onSnapshot(this.docUpdate);
        
    }
    
    render(){
        const { subMenu } = this.props.match.params;
        
        const {isLoading,invoice,invoiceValue,sender,data} = this.state;
        
        return(
            <div className="dashboard-page">
                <Row className="stat-row" gutter={24}>
                    <Col span={8}>
                        <Card >
                            <p className="card-title">Invoice Sent/Downloaded</p>
                            
                            {isLoading ? (  
                                <Skeleton.Button active style={{width:150}} />
                            ): (<Title level={3}>{invoice}</Title>)
                            }
                            <div className='ant-card-actions'>
                                <Link to="/admin/invoice">Show all invoice</Link>
                            </div>
                        </Card>
                    </Col>
                    
                    <Col span={8}>
                        <Card >
                            <p className="card-title">Total Invoice Value</p>
                            {isLoading ? (  
                                    <Skeleton.Button active style={{width:200}} />
                                ): (<Title level={3}>{this.currency(invoiceValue)}</Title>)
                                }
                            <div className='ant-card-actions'>
                                <Link to="/admin/invoice">Show all invoice</Link>
                            </div>
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card >
                            <p className="card-title">Sender</p>
                            {isLoading ? (  
                                    <Skeleton.Button active style={{width:150}} />
                                ): (<Title level={3}>{sender}</Title>)
                                }
                            <div className='ant-card-actions'>
                                <Link to="/admin/invoice/sender">Show all sender</Link>
                            </div>
                        </Card>
                    </Col>
                   
                </Row>
                <Row className="chart-row" gutter={24}>
                    <Col span={24} className="mb-24">
                        <Card>
                            {!isLoading ? (
                                <Suspense fallback={<div className="page-loading chart" ><Spin size="large" /></div>}>
                                <Chart1 data={data} />

                            </Suspense>
                            ):(
                                <div className="page-loading chart" ><Spin size="large" /></div>
                            )}
                            

                        </Card>
                    </Col>
                    <Col span={12}>
                        <Card>
                            {!isLoading ? (
                                <Suspense fallback={<div className="page-loading chart" ><Spin size="large" /></div>}>
                                <Chart2 data={data} />

                            </Suspense>
                            ):(
                                <div className="page-loading chart small" ><Spin size="large" /></div>
                            )}
                        </Card>
                    </Col>
                    <Col span={12}>
                        <Card>
                            {!isLoading ? (
                                <Suspense fallback={<div className="page-loading chart" ><Spin size="large" /></div>}>
                                <Chart3 data={data} />

                            </Suspense>
                            ):(
                                <div className="page-loading chart small" ><Spin size="large" /></div>
                            )}
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default Dashboard;